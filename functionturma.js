class Turma{
    constructor(nome, quantidade, disciplinas, professores){
        this.nome = nome;
        this.quantidade = quantidade;
        this.disciplinas = disciplinas;
        this.professores = professores;
    }
     imprimirTodosDados(){
        console.log(`      
                     ${this.nome}
                     ${this.quantidade}
                     ${this.disciplinas}
                     ${this.professores}`)
     }
    }
    let turma = new Turma("5° série", "50 alunos", "português, matemática, geografia", "Lúcia, Hugo, Artur,Alexandre");
turma.imprimirTodosDados();

class Alimentação{
    constructor(cantina, cardapio, quantidadealunos, responsaveis){
        this.cantina = cantina;
        this.cardapio = cardapio;
        this.quantidadealunos = quantidadealunos;
        this.responsaveis = responsaveis;
    }
     imprimirTodosDados(){
        console.log(`      
                     ${this.cantina}
                     ${this.cardapio}
                     ${this.quantidadealunos}
                     ${this.responsaveis}`)
     }
    }
    let alimentacao = new Alimentação("Felicidade ao Comer", "Pastel, Coxinha, Empada e Sanduíche", "300", "Dona Cida e Seu Jorge");
    alimentacao.imprimirTodosDados();

    class Disciplina{
        constructor(nome, turmas, quantidadealunos, professores){
            this.nome = nome;
            this.turmas = turmas;
            this.quantidadealunos = quantidadealunos;
            this.professores = professores;
        }
         imprimirTodosDados(){
            console.log(`      
                         ${this.nome}
                         ${this.turmas}
                         ${this.quantidadealunos}
                         ${this.professores}`)
         }
        }
        let disciplina = new Disciplina("Geografia", "6°ano, 7° ano, 8° ano, 9° ano", "150", "Lúcia, Hugo, Artur,Alexandre");
    disciplina.imprimirTodosDados();
    
    function Professor(nome, sobrenome, formações, turma,cpf, email, nascimento){
        return{
            nome,
            sobrenome,
            formações,
            turma,
            cpf,
            email,
            nascimento,
         imprimirInformaçoes: function(){
                                            {console.log( 
                                            `${this.nome}, 
                                             ${this.sobrenome},
                                             ${this.formações}, 
                                             ${this.turma}, 
                                             ${this.cpf}, 
                                             ${this.email}, 
                                             ${this.nascimento}`)} 
        }};
    }
    let professor = Professor("Aline", "Alves", "Pedagogia", "5° série", "783982398230", "aline@gmail", "12/04/1996");
    professor.imprimirInformaçoes();    


